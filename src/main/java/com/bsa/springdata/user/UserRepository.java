package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    List<User> findAllByLastNameLikeIgnoreCase(String lastname, Pageable pageable);

    @Query("SELECT u FROM User u WHERE u.office.city = :city ORDER BY u.lastName ASC")
    List<User> findAllByCity(String city);

    List<User> findByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    @Query("SELECT u " +
            "FROM User u " +
            "LEFT JOIN Team t ON u.team = t.id " +
            "LEFT JOIN Office o ON u.office = o.id " +
            "WHERE o.city = :city AND t.room = :room " +
            "ORDER BY u.lastName DESC")
    List<User> findAllByRoomAndCity(String city, String room);

    @Modifying
    @Query("DELETE FROM User u WHERE u.experience < :experience")
    int deleteAllByExperience(int experience);
}
