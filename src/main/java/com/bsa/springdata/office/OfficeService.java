package com.bsa.springdata.office;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OfficeService {
    @Autowired
    private OfficeRepository officeRepository;

    public List<OfficeDto> getByTechnology(String technology) {
        return officeRepository
                .getAllByTechnology(technology)
                .stream()
                .map(OfficeDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<OfficeDto> updateAddress(String oldAddress, String newAddress) {
        try {
            officeRepository.updateAddress(oldAddress, newAddress);

            var updatedOffice = officeRepository.getOfficeByAddress(newAddress);
            return Optional.of(OfficeDto.fromEntity(updatedOffice.get()));
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
