package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query("SELECT DISTINCT o FROM Office o LEFT JOIN User u on o.id = u.office LEFT JOIN Team t on t.id = u.team LEFT JOIN Technology tec on t.technology = tec.id WHERE tec.name = :technology")
    List<Office> getAllByTechnology(String technology);

    @Modifying
    @Transactional
    @Query("UPDATE Office o SET o.address = :newAddress WHERE o.address = :oldAddress AND o.id IN (SELECT DISTINCT o.id FROM Office o, User u WHERE u.office = o.id)")
    void updateAddress(String oldAddress, String newAddress);

    Optional<Office> getOfficeByAddress(String address);
}
