package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {
    int countByTechnologyName(String newTechnology);

    Optional<Team> findByName(String teamName);

    @Modifying
    @Transactional
    @Query(value = "UPDATE teams t " +
                    "SET technology_id = " +
                            "(SELECT tec.id FROM technologies tec WHERE tec.name = ?3) " +
                    "WHERE t.id IN " +
                            "(SELECT t.id FROM teams t WHERE t.id IN " +
                                "(SELECT iuC.id FROM " +
                                    "(SELECT t.id, COUNT(DISTINCT u.id) AS userCount " +
                                    "FROM teams t LEFT JOIN users u on t.id = u.team_id GROUP BY t.id) AS iuC " +
                                    "WHERE t.technology_id = " +
                                        "(SELECT tec.id FROM technologies tec WHERE tec.name = ?2) AND userCount < ?1))", nativeQuery = true)
    void updateTeamTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName);

    @Modifying
    @Transactional
    @Query(value = "UPDATE teams t " +
                    "SET name = CONCAT(" +
                                "t.name, " +
                                "'_', " +
                                "(SELECT p.name FROM projects p LEFT JOIN teams t2 on p.id = t2.project_id WHERE t2.name = ?1), " +
                                "'_', " +
                                "(SELECT tec.name FROM technologies tec LEFT JOIN teams t3 on tec.id = t3.technology_id  WHERE t3.name = ?1)) " +
                    "WHERE t.name = ?1", nativeQuery = true)
    void normalizeName(String teamName);
}
