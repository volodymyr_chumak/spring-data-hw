package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query("SELECT p " +
            "FROM Project p " +
            "LEFT JOIN Team t ON t.project = p.id " +
            "LEFT JOIN Technology tec ON t.technology = tec.id " +
            "WHERE tec.name = :technology ORDER BY p.teams.size DESC")
    List<Project> findTop5ByTechnology(String technology, Pageable pageable);

//    @Query("SELECT p FROM projects p WHERE p.id = (SELECT qu.id FROM (SELECT DISTINCT\n" +
//            "       id,\n" +
//            "       CASE\n" +
//            "           WHEN COUNT(*) = 1 THEN (SELECT p.id FROM projects p WHERE p.id = id LIMIT 1)\n" +
//            "           ELSE\n" +
//            "                (SELECT\n" +
//            "                     CASE\n" +
//            "                         WHEN countOfRows = 1 THEN (SELECT p.id FROM projects p WHERE p.id = id LIMIT 1)\n" +
//            "                         ELSE (SELECT p.id FROM projects p WHERE p.id = id ORDER BY p.name DESC LIMIT 1)\n" +
//            "                         END AS c\n" +
//            "                 FROM (SELECT SUM(countOfRows) AS countOfRows FROM (SELECT COUNT(*) AS countOfRows FROM (SELECT pu.id, countOfUsersOnProject FROM (SELECT p.id, COUNT(DISTINCT u.*) AS countOfUsersOnProject, RANK() OVER (ORDER BY COUNT(*) DESC) AS rank FROM projects p LEFT JOIN teams t on p.id = t.project_id LEFT JOIN users u on t.id = u.team_id WHERE p.id IN ('95a46088-b143-483d-a86c-3d6be6fd2e6d', 'ccb9f16b-e852-4ffb-b5e4-87247d978fd9') GROUP BY p.id ORDER BY countOfUsersOnProject DESC) pu WHERE rank = 1) puu GROUP BY puu.id) c) ccc)\n" +
//            "        END\n" +
//            "FROM (SELECT p.id, SUM(countOfTeamsRows) AS countOfTeamsRows FROM (SELECT COUNT(*) AS countOfTeamsRows FROM (SELECT id, countOfTeams FROM (SELECT p.id, COUNT(DISTINCT t.id) AS countOfTeams, RANK() OVER (ORDER BY COUNT(*) DESC) AS rank FROM projects p LEFT JOIN teams t on p.id = t.project_id GROUP BY p.id, t.project_id ORDER BY countOfTeams DESC) mp WHERE rank = 1 GROUP BY id, countOfTeams) pr GROUP BY pr.id) qu LEFT JOIN projects p ON p.id = LIMIT 1) qqq) qeq) ;")


    // this query works but return facebook, a test inner CASE query and it is worked.
//    @Query(value = "SELECT p FROM projects p WHERE p.id = (SELECT qu.id FROM (SELECT DISTINCT\n" +
//            "       id,\n" +
//            "       CASE\n" +
//            "           WHEN COUNT(*) = 1 THEN (SELECT p.id FROM projects p WHERE p.id = id LIMIT 1)\n" +
//            "           ELSE\n" +
//            "                (SELECT\n" +
//            "                     CASE\n" +
//            "                         WHEN countOfRows = 1 THEN (SELECT p.id FROM projects p WHERE p.id = id LIMIT 1)\n" +
//            "                         ELSE (SELECT p.id FROM projects p WHERE p.id = id ORDER BY p.name DESC LIMIT 1)\n" +
//            "                         END AS c\n" +
//            "                 FROM (SELECT SUM(countOfRows) AS countOfRows FROM (SELECT COUNT(*) AS countOfRows FROM (SELECT pu.id, countOfUsersOnProject FROM (SELECT p.id, COUNT(DISTINCT u.*) AS countOfUsersOnProject, RANK() OVER (ORDER BY COUNT(*) DESC) AS rank FROM projects p LEFT JOIN teams t on p.id = t.project_id LEFT JOIN users u on t.id = u.team_id WHERE p.id = pr.id GROUP BY p.id ORDER BY countOfUsersOnProject DESC) pu WHERE rank = 1) puu GROUP BY puu.id) c) ccc)\n" +
//            "        END\n" +
//            "FROM (SELECT id, countOfTeams FROM (SELECT p.id, COUNT(DISTINCT t.id) AS countOfTeams, RANK() OVER (ORDER BY COUNT(*) DESC) AS rank FROM projects p LEFT JOIN teams t on p.id = t.project_id GROUP BY p.id, t.project_id ORDER BY countOfTeams DESC) mp WHERE rank = 1 GROUP BY id, countOfTeams) pr GROUP BY pr.id) qu LIMIT 1)", nativeQuery = true)
    // Project findTheBiggest(Pageable pageable);


    @Query("SELECT COUNT(DISTINCT p.id) " +
            "FROM Project p " +
            "LEFT JOIN Team t ON t.project = p.id " +
            "LEFT JOIN User u ON u.team = t.id " +
            "LEFT JOIN u.roles r LEFT JOIN r.users usr " +
            "WHERE usr.id = u.id AND r.name = :role")
    int getCountWithRole(String role);

    @Query(value = "SELECT " +
            "DISTINCT p.name, " +
            "COUNT(DISTINCT t.id) AS teamsNumber, COUNT(DISTINCT u.id) AS developersNumber, " +
            "string_agg(DISTINCT tec.name, ',' ORDER BY tec.name DESC) AS technologies " +
            "FROM projects p " +
            "LEFT JOIN teams t ON p.id = t.project_id " +
            "LEFT JOIN technologies tec ON tec.id = t.technology_id " +
            "LEFT JOIN users u ON t.id = u.team_id " +
            "GROUP BY t.project_id, p.name ORDER BY p.name ASC", nativeQuery = true)
    List<ProjectSummaryDto> getProjectsSummary();


    @Query(value = "SELECT p FROM (SELECT p, COUNT(DISTINCT t.id) AS countOfTeams, RANK() OVER (ORDER BY COUNT(*) DESC) AS rank FROM projects p LEFT JOIN teams t on p.id = t.project_id GROUP BY p, t.project_id ORDER BY countOfTeams DESC) mp WHERE rank = 1 GROUP BY p", nativeQuery = true)
    List<Project> findAllProjectsWithMaxTeamCount();
}