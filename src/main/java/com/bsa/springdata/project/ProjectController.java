package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/project")
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    @GetMapping("/top5/{technology}")
    public List<ProjectDto> getTop5ByTechnology(@PathVariable String technology) {
        return projectService.findTop5ByTechnology(technology);
    }

//    @GetMapping("/biggest")
//    public ProjectDto findBiggestProject() {
//        return projectService.findTheBiggest().get();
//    }

    @GetMapping("/role/{role}")
    public int getCountWithRole(@PathVariable String role) {
        return projectService.getCountWithRole(role);
    }
}
