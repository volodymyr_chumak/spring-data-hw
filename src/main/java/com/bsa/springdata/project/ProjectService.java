package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        return projectRepository
                .findTop5ByTechnology(technology, PageRequest.of(0, 5))
                .stream()
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        // TODO: Use single query to load data. Sort by teams, developers, project name
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface

        // i try to write this sql, but working only inner CASE

//        return Optional.of(ProjectDto.fromEntity(projectRepository.findTheBiggest(PageRequest.of(0,1))));
        return Optional.empty();

    }

    public List<ProjectSummaryDto> getSummary() {
        return projectRepository.getProjectsSummary();
    }

    public int getCountWithRole(String role) {
        return projectRepository.getCountWithRole(role);
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        var technology = buildTechnology(createProjectRequest);
        var project = buildProject(createProjectRequest);
        var team = buildTeam(createProjectRequest, technology, project);

        var createdTechnology = technologyRepository.save(technology);
        var createdProject = projectRepository.save(project);
        var createdTeam = teamRepository.save(team);

        return createdTeam.getId();
    }

    private Technology buildTechnology(CreateProjectRequestDto createProjectRequest) {
        return Technology.builder()
                .name(createProjectRequest.getTech())
                .description(createProjectRequest.getTechDescription())
                .link(createProjectRequest.getTechLink())
                .build();
    }

    private Project buildProject(CreateProjectRequestDto createProjectRequest) {
        return Project.builder()
                .name(createProjectRequest.getProjectName())
                .description(createProjectRequest.getProjectDescription())
                .build();
    }

    private Team buildTeam(CreateProjectRequestDto createProjectRequest, Technology technology, Project project) {
        return Team.builder()
                .name(createProjectRequest.getTeamName())
                .area(createProjectRequest.getTeamArea())
                .room(createProjectRequest.getTeamRoom())
                .technology(technology)
                .project(project)
                .build();
    }
}
