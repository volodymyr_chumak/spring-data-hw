package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM roles r " +
            "WHERE r.code = ?1 AND r.code NOT IN " +
            "(SELECT DISTINCT r.code " +
            "FROM roles r, user2role u2r, users u " +
            "WHERE u2r.role_id = r.id AND u2r.user_id = u.id)", nativeQuery = true)
    int deleteRoleIfThereAreNoUsers(String roleCode);
}
